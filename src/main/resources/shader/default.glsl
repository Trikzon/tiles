#vertex
#version 400 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec2 uvCoords;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 transformation;

out vec2 pass_uvCoords;

void main(void)
{
    gl_Position = projection * view * transformation * vec4(vertex, 1.0);
    pass_uvCoords = uvCoords;
}

#fragment
#version 400 core

in vec2 pass_uvCoords;

uniform sampler2D textureSampler;

out vec4 out_Color;

void main(void)
{
    out_Color = texture(textureSampler, pass_uvCoords);
}
