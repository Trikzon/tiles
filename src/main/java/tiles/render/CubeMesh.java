/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: CubeMesh.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render;

import tiles.struct.Direction;

public class CubeMesh
{
    public static float[] getVertices(Direction dir)
    {
        switch (dir)
        {
            case NORTH:
                return new float[] {1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1};
            case EAST:
                return new float[] {1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0};
            case SOUTH:
                return new float[] {0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0};
            case WEST:
                return new float[] {0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1};
            case UP:
                return new float[] {0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1};
            case DOWN:
                return new float[] {0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1};
        }
        return null;
    }

    public static int[] getIndices(Direction dir)
    {
        return new int[] {0, 1, 2, 0, 2, 3};
    }

    public static float[] getUvCoords(Direction dir)
    {
        return new float[] {0, 0, 1, 0, 1, 1, 0, 1};
    }
}
