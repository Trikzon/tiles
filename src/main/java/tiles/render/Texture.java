/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Texture.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render;

public class Texture
{
    public int atlasX, atlasY, atlasSize;
    public int size;

    public Texture(int size, int atlasSize)
    {
        this.size = size;
        this.atlasSize = atlasSize;
    }
}
