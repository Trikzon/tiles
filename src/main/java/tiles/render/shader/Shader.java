/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Shader.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render.shader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

import tiles.struct.Struct3f;

public abstract class Shader
{
    public int gl_program;
    public int gl_vertex;
    public int gl_fragment;

    public Shader(String location)
    {
        String source = "";
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(
                        "src/main/resources/shader/" + location
            ));
            String line;
            while ((line = reader.readLine()) != null)
            {
                source += line + "\n";
            }
            reader.close();
        }
        catch (IOException e)
        {
            // TODO: Log this error
            System.err.println("Could not read file!");
            e.printStackTrace();
            System.exit(-1);
        }

        this.gl_vertex = compile(source, ShaderType.VERTEX);
        this.gl_fragment = compile(source, ShaderType.FRAGMENT);
        this.gl_program = GL20.glCreateProgram();
        GL20.glAttachShader(this.gl_program, this.gl_vertex);
        GL20.glAttachShader(this.gl_program, this.gl_fragment);

        bindAttributes();
        GL20.glLinkProgram(this.gl_program);
        GL20.glValidateProgram(this.gl_program);
        getUniformLocations();
    }

    public abstract void bindAttributes();

    public void bindAttribute(int attribute, String name)
    {
        GL20.glBindAttribLocation(gl_program, attribute, name);
    }

    public abstract void getUniformLocations();

    public int getUniformLocation(String name)
    {
        return GL20.glGetUniformLocation(gl_program, name);
    }

    public void loadBoolean(int location, boolean value)
    {
        GL20.glUniform1f(location, value ? 1 : 0);
    }

    public void loadFloat(int location, float value)
    {
        GL20.glUniform1f(location, value);
    }

    public void loadStruct3f(int location, Struct3f struct)
    {
        GL20.glUniform3f(location, struct.x, struct.y, struct.z);
    }

    private static final FloatBuffer matrixBuffer = BufferUtils.createFloatBuffer(16);
    public void loadMatrix(int location, Matrix4f matrix)
    {
        GL20.glUniformMatrix4fv(location, false, matrix.get(matrixBuffer));
    }

    public void start()
    {
        GL20.glUseProgram(gl_program);
    }

    public void stop()
    {
        GL20.glUseProgram(0);
    }

    public void close()
    {
        stop();
        GL20.glDetachShader(gl_program, gl_vertex);
        GL20.glDetachShader(gl_program, gl_fragment);
        GL20.glDeleteShader(gl_vertex);
        GL20.glDeleteShader(gl_fragment);
        GL20.glDeleteProgram(gl_program);
    }

    public static int compile(String source, ShaderType type)
    {
        int gl_shader = GL20.glCreateShader(type.gl_type);
        GL20.glShaderSource(gl_shader, type.subSource(source));
        GL20.glCompileShader(gl_shader);
        if (GL20.glGetShaderi(gl_shader, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE)
        {
            // TODO: Log this error
            System.out.println(GL20.glGetShaderInfoLog(gl_shader));
            System.err.println("Could not compile shader.");
            System.exit(-1);
        }
        return gl_shader;
    }

    private static enum ShaderType
    {
        VERTEX("#vertex", GL20.GL_VERTEX_SHADER),
        FRAGMENT("#fragment", GL20.GL_FRAGMENT_SHADER);

        public String indicator;
        public int gl_type;

        ShaderType(String indicator, int gl_type)
        {
            this.indicator = indicator;
            this.gl_type = gl_type;
        }

        public ShaderType nextType()
        {
            boolean found = false;
            for (ShaderType type : ShaderType.values())
            {
                if (found)
                {
                    return type;
                }
                if (type == this)
                {
                    found = true;
                }
            }
            return null;
        }

        public String subSource(String fullSource)
        {
            ShaderType nextType = nextType();
            return fullSource.substring(
                    fullSource.indexOf(indicator) + indicator.length(),
                    nextType != null ? fullSource.indexOf(nextType().indicator) : fullSource.length()
            );
        }
    }
}
