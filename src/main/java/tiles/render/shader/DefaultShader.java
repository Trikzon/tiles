/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: DefaultShader.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render.shader;

import org.joml.Matrix4f;

import tiles.render.Camera;

public class DefaultShader extends Shader
{
    public int uni_projection;
    public int uni_view;
    public int uni_transformation;

    public DefaultShader()
    {
        super("default.glsl");
    }

    @Override
    public void bindAttributes() {
        bindAttribute(0, "vertex");
        bindAttribute(1, "uvCoords");
    }

    @Override
    public void getUniformLocations() {
        this.uni_projection = getUniformLocation("projection");
        this.uni_view = getUniformLocation("view");
        this.uni_transformation = getUniformLocation("transformation");
    }

    public void loadProjection(Matrix4f matrix)
    {
        super.loadMatrix(this.uni_projection, matrix);
    }

    public void loadView(Camera camera)
    {
        Matrix4f matrix = Camera.createView(camera);
        super.loadMatrix(this.uni_view, matrix);
    }

    public void loadTransformation(Matrix4f matrix)
    {
        super.loadMatrix(this.uni_transformation, matrix);
    }
}
