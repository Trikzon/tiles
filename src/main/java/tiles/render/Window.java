/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Window.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render;

import org.lwjgl.glfw.Callbacks;
import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;

public class Window
{
    public static Window inst;

    public final long gl_window;
    public int width, height;
    public String title;

    public Window(int width, int height, String title)
    {
        if (Window.inst != null)
        {
            throw new RuntimeException("Window singleton already initialized");
        }
        Window.inst = this;
        this.width = width;
        this.height = height;
        this.title = title;

        GLFWErrorCallback.createPrint(System.err).set();

        if (!GLFW.glfwInit())
        {
            throw new IllegalStateException("Unable to initialize GLFW");
        }
        GLFW.glfwDefaultWindowHints();
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE);
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE);
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 3);
        GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 2);
        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE);
        GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, GLFW.GLFW_TRUE);

        this.gl_window = GLFW.glfwCreateWindow(width, height, title, 0, 0);
        if (gl_window == 0)
        {
            throw new RuntimeException("Failed to create the GLFW window.");
        }

        GLFWVidMode vidMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
        GLFW.glfwSetWindowPos(
                gl_window,
                (vidMode.width() - width) / 2,
                (vidMode.height() - height) / 2
        );

        GLFW.glfwMakeContextCurrent(gl_window);
        GLFW.glfwSwapInterval(1);
        GLFW.glfwShowWindow(gl_window);
        GL.createCapabilities();
    }

    public static void update()
    {
        GLFW.glfwSwapBuffers(Window.inst.gl_window);
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        GLFW.glfwPollEvents();
    }

    public static void close()
    {
        long gl_window = Window.inst.gl_window;
        Callbacks.glfwFreeCallbacks(gl_window);
        GLFW.glfwDestroyWindow(gl_window);
        GLFW.glfwSetErrorCallback(null).free();
    }

    public static boolean shouldClose()
    {
        return GLFW.glfwWindowShouldClose(Window.inst.gl_window);
    }

    public static void setClearColor(float r, float g, float b, float a)
    {
        GL11.glClearColor(r, g, b, a);
    }

    public static boolean isKeyPressed(int gl_keyCode)
    {
        return GLFW.glfwGetKey(Window.inst.gl_window, gl_keyCode) == GLFW.GLFW_PRESS;
    }
}
