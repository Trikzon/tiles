/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Camera.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render;

import org.joml.Matrix4f;
import org.lwjgl.glfw.GLFW;

import tiles.render.shader.DefaultShader;
import tiles.struct.Struct3f;

public class Camera
{
    public static final float FOV = 70f;
    public static final float NEAR_PLANE = 0.1f;
    public static final float FAR_PLANE = 1000f;

    public Struct3f pos, rot;
    public Matrix4f projection;

    public Camera(DefaultShader shader, Struct3f pos, Struct3f rot)
    {
        this.pos = pos;
        this.rot = rot;
        this.projection = createProjection();
        shader.start();
        shader.loadProjection(this.projection);
        shader.stop();
    }

    public Camera(DefaultShader shader)
    {
        this(shader, new Struct3f(), new Struct3f());
    }

    private static Matrix4f createProjection()
    {
        float aspectRatio = (float) Window.inst.width / (float) Window.inst.height;
        float yScale = (float) ((1f / Math.tan(Math.toRadians(FOV / 2f))) * aspectRatio);
        float xScale = yScale / aspectRatio;
        float frustumLength = FAR_PLANE - NEAR_PLANE;

        Matrix4f matrix = new Matrix4f();
        matrix.m00(xScale);
        matrix.m11(yScale);
        matrix.m22(-((FAR_PLANE + NEAR_PLANE) / frustumLength));
        matrix.m23(-1);
        matrix.m32(-((2 * NEAR_PLANE * FAR_PLANE) / frustumLength));
        matrix.m33(0);
        return matrix;
    }

    public static Matrix4f createView(Camera camera)
    {
        Matrix4f matrix = new Matrix4f();
        matrix.identity();
        matrix.rotateXYZ(
                (float) Math.toRadians(camera.rot.x),
                (float) Math.toRadians(camera.rot.y),
                (float) Math.toRadians(camera.rot.z)
        );
        Struct3f negCameraPos = new Struct3f(camera.pos).negate();
        matrix.translate(negCameraPos.x, negCameraPos.y, negCameraPos.z);
        return matrix;
    }

    public static Matrix4f createTransformation(Struct3f translation, Struct3f rotation, float scale)
    {
        Matrix4f matrix = new Matrix4f();
        matrix.identity();
        matrix.translate(translation.x, translation.y, translation.z);
        matrix.rotateXYZ(
                (float) Math.toRadians(rotation.x),
                (float) Math.toRadians(rotation.y),
                (float) Math.toRadians(rotation.z)
        );
        matrix.scale(scale); // Scales by x, y, and z uniformly
        return matrix;
    }

    /**
     * Reads input and moves the camera respectively
     * @return true if pos or rot change
     */
    public boolean input()
    {
        Struct3f prevPos = new Struct3f(this.pos);
        Struct3f prevRot = new Struct3f(this.rot);

        if (Window.isKeyPressed(GLFW.GLFW_KEY_W))
        {
            pos.z -= 0.1;
        }
        if (Window.isKeyPressed(GLFW.GLFW_KEY_S))
        {
            pos.z += 0.1;
        }
        if (Window.isKeyPressed(GLFW.GLFW_KEY_A))
        {
            pos.x -= 0.1;
        }
        if (Window.isKeyPressed(GLFW.GLFW_KEY_D))
        {
            pos.x += 0.1;
        }
        if (Window.isKeyPressed(GLFW.GLFW_KEY_SPACE))
        {
            pos.y += 0.1;
        }
        if (Window.isKeyPressed(GLFW.GLFW_KEY_LEFT_SHIFT))
        {
            pos.y -= 0.1;
        }
        if (Window.isKeyPressed(GLFW.GLFW_KEY_DOWN))
        {
            rot.x += 1;
        }
        if (Window.isKeyPressed(GLFW.GLFW_KEY_UP))
        {
            rot.x -= 1;
        }
        if (Window.isKeyPressed(GLFW.GLFW_KEY_RIGHT))
        {
            rot.y += 1;
        }
        if (Window.isKeyPressed(GLFW.GLFW_KEY_LEFT))
        {
            rot.y -= 1;
        }

        return !prevPos.equals(pos) || !prevRot.equals(rot);
    }
}
