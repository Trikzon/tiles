/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Mesh.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import tiles.struct.Struct3f;

public class Mesh
{
    public int gl_vao;
    public ArrayList<Integer> gl_vbos;
    public int vertexCount;
    public Struct3f rotation = new Struct3f();

    public Mesh(float[] vertices, float[] uvCoords, int[] indices)
    {
        this.vertexCount = indices.length;

        gl_vao = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(gl_vao);

        gl_vbos = new ArrayList<>();
        gl_vbos.add(bindIndices(indices));
        gl_vbos.add(storeAttribute(0, 3, vertices));
        gl_vbos.add(storeAttribute(1, 2, uvCoords));

        GL30.glBindVertexArray(0);
    }

    private static int bindIndices(int[] indices)
    {
        int gl_vbo = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, gl_vbo);
        IntBuffer buffer = BufferUtils.createIntBuffer(indices.length);
        buffer.put(indices);
        buffer.flip();
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        return gl_vbo;
    }

    private static int storeAttribute(int attributeNumber, int size, float[] data)
    {
        int gl_vbo = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, gl_vbo);
        FloatBuffer buffer = BufferUtils.createFloatBuffer(data.length);
        buffer.put(data);
        buffer.flip();
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(attributeNumber, size, GL11.GL_FLOAT, false, 0, 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        return gl_vbo;
    }

    public void close()
    {
        GL30.glDeleteVertexArrays(gl_vao);
        gl_vbos.forEach(GL30::glDeleteBuffers);
    }
}
