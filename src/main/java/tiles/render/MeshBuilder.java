/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: MeshBuilder.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render;

import java.util.ArrayList;

import tiles.util.ArrayUtil;

public class MeshBuilder
{
    public ArrayList<Float> vertices = new ArrayList<>();
    public ArrayList<Float> uvCoords = new ArrayList<>();
    public ArrayList<Integer> indices = new ArrayList<>();

    public MeshBuilder()
    {

    }

    public void add(float[] vertices, float[] uvCoords, int[] indices, Texture texture, float x, float y, float z)
    {
        // offset indices
        int indiceOffset = this.vertices.size() / 3;
        for (int indice : indices)
        {
            indice += indiceOffset;
            this.indices.add(indice);
        }

        // offset vertices
        int counter = 0;
        for (int i = 0; i < vertices.length; i++)
        {
            float vertice = vertices[i];
            if (counter == 0)
            {
                this.vertices.add(vertice + x);
            }
            else if (counter == 1)
            {
                this.vertices.add(vertice + y);
            }
            else if (counter == 2)
            {
                this.vertices.add(vertice + z);
            }
            counter++;
            if (counter == 3) counter = 0;
        }

        // offset uvCoords
        float scale = (float) texture.atlasSize / texture.size;
        float chunkUnit = (float) 16 / texture.atlasSize;
        for (int i = 0; i < uvCoords.length; i++)
        {
            float newCoord;
            if (i % 2 == 0)
            {
                newCoord = (uvCoords[i] / scale) + (chunkUnit * texture.atlasX);
            }
            else
            {
                newCoord = (uvCoords[i] / scale) + (chunkUnit * texture.atlasY);
            }
            this.uvCoords.add(newCoord);
        }
    }

    public Mesh build()
    {
        float[] vertArray = ArrayUtil.toFloatArray(vertices);
        float[] uvArray = ArrayUtil.toFloatArray(uvCoords);
        int[] indiceArray = ArrayUtil.toIntArray(indices);

        return new Mesh(vertArray, uvArray, indiceArray);
    }
}
