/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: TextureAtlas.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render;

import java.awt.image.BufferedImage;
import java.awt.Graphics;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

public class TextureAtlas
{
    public int gl_texture;
    public int size;

    private HashMap<Texture, BufferedImage> rawImages = new HashMap<>();

    public TextureAtlas(int size)
    {
        this.size = size;
    }

    public Texture loadTexture(String location)
    {
        if (rawImages == null)
        {
            // Log this
            System.out.println("Attempted to load texture: " + location + " to already stitched atlas.");
        }
        BufferedImage image = null;
        try
        {
            image = ImageIO.read(new File("src/main/resources/texture/" + location));
        }
        catch (IOException e)
        {
            // TODO: Log this error
            e.printStackTrace();
        }

        if (image != null)
        {
            if (image.getHeight() != image.getWidth())
            {
                // TODO: Log that images must be square
            }
            else
            {
                Texture texture = new Texture(image.getWidth(), this.size);
                rawImages.put(texture, image);
                return texture;
            }
        }
        return null;
    }

    public void stitch()
    {
        int size = pixelToChunk(this.size);
        boolean[][] chunks = new boolean[size][size];
        BufferedImage atlasImage = new BufferedImage(this.size, this.size, BufferedImage.TYPE_INT_ARGB);
        Graphics atlasGraphics = atlasImage.getGraphics();

        for (Texture texture : rawImages.keySet())
        {
            BufferedImage image = rawImages.get(texture);
            int imageChunkSize = pixelToChunk(image.getWidth());

            int[] pos = findPosToPlace(chunks, imageChunkSize);
            if (pos == null)
            {
                continue;
            }
            texture.atlasX = pos[0];
            texture.atlasY = pos[1];

            for (int x = pos[0]; x < pos[0] + imageChunkSize; x++)
            {
                for (int y = pos[1]; y < pos[1] + imageChunkSize; y++)
                {
                    chunks[x][y] = true;
                }
            }

            atlasGraphics.drawImage(image, pos[0] * 16, pos[1] * 16, null);
            image.getGraphics().dispose();
            image.flush();
        }
        atlasGraphics.dispose();
        //rawImages = null;

        int[] pixels = new int[this.size * this.size];
        atlasImage.getRGB(0, 0, this.size, this.size, pixels, 0, this.size);

        ByteBuffer buffer = BufferUtils.createByteBuffer(this.size * this.size * 4);
        for (int y = 0; y < this.size; y++)
        {
            for (int x = 0; x < this.size; x++)
            {
                int pixel = pixels[y * this.size + x];
                buffer.put((byte) ((pixel >> 16) & 0xFF)); // Red
                buffer.put((byte) ((pixel >> 8) & 0xFF));  // Green
                buffer.put((byte) (pixel & 0xFF));         // Blue
                buffer.put((byte) ((pixel >> 24) & 0xFF)); // Alpha
            }
        }
        buffer.flip();

        gl_texture = GL11.glGenTextures();
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, gl_texture);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
        GL11.glTexImage2D(
                GL11.GL_TEXTURE_2D, 0, GL11.GL_RGBA, this.size, this.size,
                0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer
        );

        try
        {
            ImageIO.write(atlasImage, "png", new File("src/main/resources/texture/atlas.png"));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private int pixelToChunk(int pixelSize)
    {
        int chunkSize = pixelSize / 16;
        chunkSize += pixelSize % 16 == 0 ? 0 : 1;
        return chunkSize;
    }

    private int[] findPosToPlace(boolean[][] chunks, int chunkSize)
    {
        for (int x = 0; x < chunks.length; x++)
        {
            for (int y = 0; y < chunks[0].length; y++)
            {
                if (chunks[x][y])
                {
                    continue;
                }

                boolean fits = true;
                for (int i = x; i < chunkSize + x; i++)
                {
                    for (int j = y; j < chunkSize + y; j++)
                    {
                        if (i >= chunks.length || j >= chunks[0].length)
                        {
                            fits = false;
                            continue;
                        }
                        if (chunks[i][j])
                        {
                            fits = false;
                        }
                    }
                }

                if (fits)
                {
                    return new int[] {x, y};
                }
            }
        }
        // TODO: Log this error
        System.out.println("Couldn't fit all textures on texture atlas!");
        return null;
    }
}
