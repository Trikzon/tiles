/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Renderer.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.render;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import tiles.render.shader.DefaultShader;
import tiles.struct.Struct3f;
import tiles.world.Chunk;
import tiles.world.World;

public class Renderer
{
    static boolean once = false;
    public static void render(World world, DefaultShader shader, TextureAtlas atlas)
    {
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, atlas.gl_texture);

        for (Chunk chunk : world.chunks.values())
        {
            Mesh mesh = chunk.mesh;
            GL30.glBindVertexArray(mesh.gl_vao);
            GL20.glEnableVertexAttribArray(0);
            GL20.glEnableVertexAttribArray(1);

            Struct3f pos = chunk.toWorldSpace(new Struct3f(0, 0, 0));
            if (!once)
                System.out.println(pos);
            Matrix4f transformation = Camera.createTransformation(pos, mesh.rotation, 1);
            shader.loadTransformation(transformation);
            GL11.glDrawElements(GL11.GL_TRIANGLES, mesh.vertexCount, GL11.GL_UNSIGNED_INT, 0);
//            GL11.glDrawElements(GL11.GL_LINES, mesh.vertexCount, GL11.GL_UNSIGNED_INT, 0);

            GL20.glDisableVertexAttribArray(0);
            GL20.glDisableVertexAttribArray(1);
            GL30.glBindVertexArray(0);
        }
        once = true;
    }
}
