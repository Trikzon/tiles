/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Direction.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.struct;

public enum Direction
{
    NORTH,
    EAST,
    SOUTH,
    WEST,
    UP,
    DOWN;

    public Struct3i moveInDirection(Struct3i struct)
    {
        switch (this)
        {
            case NORTH:
                return struct.add(0, 0, 1);
            case EAST:
                return struct.add(1, 0, 0);
            case SOUTH:
                return struct.add(0, 0, -1);
            case WEST:
                return struct.add(-1, 0, 0);
            case UP:
                return struct.add(0, 1, 0);
            case DOWN:
                return struct.add(0, -1, 0);
            default:
                return struct;
        }
    }
}
