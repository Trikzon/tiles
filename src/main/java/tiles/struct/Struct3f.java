/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Struct3f.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.struct;

public class Struct3f
{
    public float x, y, z;

    public Struct3f(float x, float y, float z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Struct3f(Struct3f struct)
    {
        this.x = struct.x;
        this.y = struct.y;
        this.z = struct.z;
    }

    public Struct3f()
    {
        this(0.0f, 0.0f, 0.0f);
    }

    public Struct3f add(float x, float y, float z)
    {
        this.x += x;
        this.y += y;
        this.z += z;
        return this;
    }

    public Struct3f negate()
    {
        this.x = -x;
        this.y = -y;
        this.z = -z;
        return this;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof Struct3f))
        {
            return false;
        }
        Struct3f struct = (Struct3f) obj;
        return struct.x == this.x && struct.y == this.y && struct.z == this.z;
    }

    @Override
    public String toString() {
        return "{" + x + "," + y + "," + z + "}";
    }
}
