
/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Struct3i.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.struct;

public class Struct3i
{
    public int x, y, z;

    public Struct3i(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Struct3i()
    {
        this(0, 0, 0);
    }

    public Struct3i add(int x, int y, int z)
    {
        this.x += x;
        this.y += y;
        this.z += z;
        return this;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof Struct3i)) return false;
        Struct3i struct = (Struct3i) o;
        return struct.x == this.x && struct.y == this.y && struct.z == this.z;
    }
}
