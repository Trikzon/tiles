/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Chunk.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.world;

import tiles.render.CubeMesh;
import tiles.render.Mesh;
import tiles.render.MeshBuilder;
import tiles.struct.Direction;
import tiles.struct.Struct3f;
import tiles.struct.Struct3i;
import tiles.tile.Tile;
import tiles.tile.Tiles;

public class Chunk
{
    public static final int CHUNK_DIM = 16;

    public int chunkX, chunkY, chunkZ;
    public Tile[][][] tiles = new Tile[CHUNK_DIM][CHUNK_DIM][CHUNK_DIM];
    public Mesh mesh;
    public boolean isDirty;

    public Chunk(int chunkX, int chunkY, int chunkZ)
    {
        this.chunkX = chunkX;
        this.chunkY = chunkY;
        this.chunkZ = chunkZ;

        for (int x = 0; x < CHUNK_DIM; x++)
        {
            for (int y = 0; y < CHUNK_DIM; y++)
            {
                for (int z = 0; z < CHUNK_DIM; z++)
                {
                    tiles[x][y][z] = Tiles.AIR;

                    if (y == 0)
                    {
                        tiles[x][y][z] = Tiles.DIRT;
                    }
                }
            }
        }
        createMesh();
    }

    public void clean()
    {
        if (!isDirty) return;

        mesh.close();
        createMesh();
    }

    private void createMesh()
    {
        MeshBuilder builder = new MeshBuilder();
        for (int x = 0; x < CHUNK_DIM; x++)
        {
            for (int y = 0; y < CHUNK_DIM; y++)
            {
                for (int z = 0; z < CHUNK_DIM; z++)
                {
                    Tile tile = tiles[x][y][z];
                    if (tile.isFullCube())
                    {
                        for (Direction dir : Direction.values())
                        {
                            Struct3i offsetPos = dir.moveInDirection(new Struct3i(x, y, z));
                            boolean addMesh = false;
                            if (offsetPos.x >= CHUNK_DIM || offsetPos.y >= CHUNK_DIM || offsetPos.z >= CHUNK_DIM)
                            {
                                addMesh = true;
                            }
                            if (offsetPos.x < 0 || offsetPos.y < 0 || offsetPos.z < 0)
                            {
                                addMesh = true;
                            }
                            if (!addMesh) // this means x, y, z are in the array
                            {
                                Tile offsetTile = tiles[offsetPos.x][offsetPos.y][offsetPos.z];
                                addMesh = !tile.isSideCulled(offsetTile, dir);
                            }
                            if (addMesh)
                            {
                                builder.add(
                                        CubeMesh.getVertices(dir), CubeMesh.getUvCoords(dir), CubeMesh.getIndices(dir),
                                        tile.texture, x, y, z
                                );
                            }
                        }
                    }
                    else
                    {
                        // TODO: Support custom models
                    }
                }
            }
        }
        mesh = builder.build();
    }

    public Struct3f toWorldSpace(Struct3f pos)
    {
        return new Struct3f(
                (chunkX * CHUNK_DIM) + pos.x,
                (chunkY * CHUNK_DIM) + pos.y,
                (chunkZ * CHUNK_DIM) + pos.z
        );
    }
}
