/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: World.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.world;

import java.util.HashMap;

import tiles.struct.Struct3f;
import tiles.struct.Struct3i;

public class World
{
    public HashMap<Struct3i, Chunk> chunks = new HashMap<>();

    public World()
    {
        for (int x = 0; x < 10; x++)
        {
            for (int y = 0; y < 1; y++)
            {
                for (int z = 0; z < 10; z++)
                {
                    chunks.put(new Struct3i(x, y, -z), new Chunk(x, y, -z));
                }
            }
        }
        Chunk chunk = getChunk(new Struct3i(0, 0, -3));
        System.out.println(chunk.toWorldSpace(new Struct3f(0, 0, 0)));
    }

    public Chunk getChunk(Struct3i pos)
    {
        for (Struct3i chunkPos : chunks.keySet())
        {
            if (pos.equals(chunkPos))
            {
                return chunks.get(chunkPos);
            }
        }
        return null;
    }
}
