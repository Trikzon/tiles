/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: AirTile.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles.tile;

import tiles.struct.Direction;

public class AirTile extends Tile
{
    public AirTile()
    {
        super(null);
    }

    @Override
    public boolean isSideCulled(Tile tile, Direction direction) {
        return true;
    }

    @Override
    public boolean isFullCube() {
        return false;
    }
}
