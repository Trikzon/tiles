/* ===========================================================================
 * Copyright 2020 Trikzon
 *
 * tiles is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * File: Main.java
 * Date: 2020-05-05 "Chunks"
 * Revision:
 * Author: Trikzon
 * =========================================================================== */
package tiles;

import org.lwjgl.glfw.GLFW;

import tiles.render.Camera;
import tiles.render.Renderer;
import tiles.render.Texture;
import tiles.render.TextureAtlas;
import tiles.render.Window;
import tiles.render.shader.DefaultShader;
import tiles.tile.Tiles;
import tiles.world.World;

public class Main
{
    public static void main(String[] args)
    {
        new Window(1280, 720, "Tiles");
        Window.setClearColor(0, 0, 0, 0);
        DefaultShader shader = new DefaultShader();
        Camera camera = new Camera(shader);

        TextureAtlas tileAtlas = new TextureAtlas(1024);
        tileAtlas.loadTexture("cobblestone.png");
        Texture dirtTexture = tileAtlas.loadTexture("dirt.png");
        tileAtlas.loadTexture("stone.png");
        tileAtlas.stitch();
        Tiles.register(dirtTexture);

        float[] vertices = {
            0, 0, 0, // 0
            1, 0, 0, // 1
            1, 0, 1, // 2
            0, 0, 1, // 3
            0, 1, 0, // 4
            1, 1, 0, // 5
            1, 1, 1, // 6
            0, 1, 1  // 7
        };
        int[] indices = {
            4, 1, 5,
            4, 0, 1,

            7, 0, 4,
            7, 3, 0,

            6, 3, 7,
            6, 2, 3,

            5, 2, 6,
            5, 1, 2,

            4, 5, 6,
            4, 6, 7,

            0, 1, 2,
            0, 2, 3
        };
        float[] uvCoords = {
            0, 1,
            1, 1,
            0, 1,
            1, 1,
            0, 0,
            1, 0,
            0, 0,
            1, 0
        };

        World world = new World();

        camera.pos.add(0, 1, 0);

        double lastTime = GLFW.glfwGetTime();
        int frames = 0;
        int secondsSinceBad = 0;
        while(!Window.shouldClose())
        {
            double currentTime = GLFW.glfwGetTime();
            frames++;
            if (currentTime - lastTime >= 1.0)
            {
                if (frames >= 60)
                {
                    secondsSinceBad++;
                }
                else
                {
                    System.out.println(frames + " fps; " + 1000.0 / frames + " ms");
                    System.out.println(secondsSinceBad + " seconds since last bad fps.");
                    secondsSinceBad = 0;
                    frames = 0;
                    lastTime += 1.0;
                }
            }

            camera.input();
            Window.update();

            shader.start();
            shader.loadView(camera);
            Renderer.render(world, shader, tileAtlas);
            shader.stop();
        }

        Window.close();
    }
}
